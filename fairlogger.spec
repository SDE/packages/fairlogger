Name: fairlogger
Version: 1.9.3
Release: 2%{?dist}
Summary: Lightweight and fast C++ Logging Library

License: LGPLv3
%define github github.com
%define gh_user FairRootGroup
%define gh_repo FairLogger
%define gh_repo_url https://%{github}/%{gh_user}/%{gh_repo}
URL: %{gh_repo_url}
Source0: %{name}-%{version}.tar.gz

BuildRequires: cmake
BuildRequires: fmt-devel
BuildRequires: gcc-c++
BuildRequires: git

%description
FairLogger is a lightweight and fast C++ logging library.

%global debug_package %{nil}

%prep
%autosetup

%build
cmake -S. -Bbuild \
      -DCMAKE_INSTALL_PREFIX=%{_prefix} \
      -DCMAKE_BUILD_TYPE=Release \
      -DDISABLE_COLOR=ON \
      -DUSE_EXTERNAL_FMT=ON
cmake --build build %{?_smp_mflags}

%install
DESTDIR=%{buildroot} cmake --build build --target install

%files
%license LICENSE
%{_libdir}/libFairLogger.so*


%package devel
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: fmt-devel
Summary: Development files for %{name}

%description devel
This package contains the header files and CMake package for developing against FairLogger.

%files devel
%license LICENSE
%dir %{_includedir}/%{name}
%{_includedir}/%{name}/*.h
%dir %{_libdir}/cmake
%dir %{_libdir}/cmake/%{gh_repo}-%{version}
%{_libdir}/cmake/%{gh_repo}-%{version}/%{gh_repo}*.cmake


%changelog
* Sun May  2 2021 Dennis Klein <d.klein@gsi.de> - 1.9.3-2
- Include directories
* Tue Apr 20 2021 Dennis Klein <d.klein@gsi.de> - 1.9.3-1
- Bump v1.9.3
* Mon Jun  8 2020 Dennis Klein <d.klein@gsi.de> - 1.6.2-2
- Depend on lib package
* Mon Jun  8 2020 Dennis Klein <d.klein@gsi.de> - 1.6.2-1
- Package v1.6.2
